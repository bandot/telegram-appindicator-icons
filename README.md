# Telegram AppIndicator for Ubuntu

Custom Telegram appindicator (system tray) icons to match Unity desktop panel color scheme

## Features
* Support idle (no unread messages)
* Support unread messages
* Support unread messages (muted chat)
* Up to 1000 unread messages support

## Screenshot
![](screenshot/telegram-ubuntu-panel.png)

## Installing

*  Download the package:

`git clone https://gitlab.com/bandot/telegram-appindicator-icons.git`

* Copy the downloaded to **.local/share/TelegramDesktop/tdata/** (use *Ctrl+H* to show the hidden files)
* You can delete or backup by renaming the original one
* Open Telegram desktop app
* Enjoy

## Notes
If the message out of custom icon number, it will be automatically generated with default Telegram style

## Built With
* [Inkscape](https://inkscape.org) - Free and open-source vector graphics editor
* [Ubuntu](https://design.ubuntu.com/font) - Font family

## License
This project is licensed under the GPLv3 - see the [LICENSE.md](LICENSE.md) file for details